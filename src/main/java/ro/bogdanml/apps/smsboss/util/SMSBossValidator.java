package ro.bogdanml.apps.smsboss.util;

import org.springframework.util.StringUtils;

public class SMSBossValidator {

	/**
	 * Accepted phone number formats :
	 * 
	 * +40729403533 ( 12 digits )
	 * 123456789012 
	 * 
	 * 0729403533 ( 10 digits )
	 * 1234567890
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public static boolean phoneNumberValid(String phoneNumber) {
		
		if( StringUtils.isEmpty(phoneNumber))
			return false;
		
		if(phoneNumber.length() < 10)
			return false;
		
		if(phoneNumber.startsWith("+"))
		{
			if(phoneNumber.length() - 3 != 9 )
				return false;
		}
		
		
		return true;
	}

}
