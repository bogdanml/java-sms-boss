package ro.bogdanml.apps.smsboss;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.OutboundMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ro.bogdanml.apps.smsboss.bean.SMSBossOutboundMessage;
import ro.bogdanml.apps.smsboss.config.SMSBossConfig;
import ro.bogdanml.apps.smsboss.exception.SMSBossValidationException;
import ro.bogdanml.apps.smsboss.util.SMSBossValidator;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.Date;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class SMSBossScheduler {


	private static final Logger logger = LoggerFactory.getLogger(SMSBossScheduler.class);
	
	@Autowired
	private SMSBossService smsBossService;
	
	@Autowired
	private SMSBossConfig smsBossConfig;

	private ExecutorService senderExecutor;

	private Queue<SMSBossOutboundMessage> scheduledSMSs = new ConcurrentLinkedQueue<SMSBossOutboundMessage>();

	
	private void sendMessages() {
		
		
		Date currentTime = Calendar.getInstance().getTime();
		int maxDeliveries = scheduledSMSs.size();
		int i = 0;
		
		logger.debug("Trying to deliver  {} scheduled SMSs", maxDeliveries);
		
		while (i < maxDeliveries) {

			SMSBossOutboundMessage message = scheduledSMSs.poll();

			if ( currentTime.after(message.getDeliveDate()) 
					&& !smsBossService.sendSmsMessage(message)) 
			{
				//Put it back in the pot for redelivery on next cycle;
				scheduledSMSs.add(message);
			}
			i++;
		}
		
	}
	
	@PostConstruct
	private void init() {			
		
		//Nu are sens sa folosesc spring pentru un thread amarat care ruleaza la un interval  fix;
		// SAU decuplare configurare  spring / aplicatii;
		senderExecutor = Executors.newSingleThreadExecutor();
		
		senderExecutor.execute( new Thread() {
			@Override
			public void run() {
				
				long sleepTime = smsBossConfig.getDeliveryInterval();
				
				while(true) {
					try { 
							Thread.sleep(sleepTime);
							sendMessages();
					}
					catch(Throwable t) {
						logger.error("Error  sending scheduled messages  {} ", t.getMessage(), t);
					}
				}
				
			}});
		/*
		executor.scheduleAtFixedRate(new Thread() {
			@Override
			public void run() {
				sendMessages();
			}
		}, 1000L);*/

		if(!StringUtils.isEmpty(smsBossConfig.getBootSMSNumber()))
			try {
				scheduleSmsForDelivery(smsBossConfig.getBootSMSNumber(), "SMSBoss is  up and running ! [" + Calendar.getInstance().getTime() +"]");
				//scheduleSmsForDelivery("1515", "How much is the fish?");
			} catch (SMSBossValidationException e) {
				logger.error("Error scheduling BOOT SMS MESSAGE{} ", e);
			}
	}
	
	/**
	 * Schedules a sms right now for delivery
	 * @param recipient
	 * @param content
	 * @throws SMSBossValidationException
	 */
	public void scheduleSmsForDelivery(String recipient, String content) throws SMSBossValidationException {
		scheduleSmsForDelivery(recipient, content, new Date());
	}

	/**
	 * Schedules a sms for later delivery
	 * @param recipient
	 * @param content
	 * @param deliveryDate
	 * @throws SMSBossValidationException
	 */
	public void scheduleSmsForDelivery(String recipient, String content, Date deliveryDate) throws SMSBossValidationException {
		
		if(!isSpecialNumber(recipient))
			validateSMSRequest(recipient, content, deliveryDate);

		OutboundMessage outboundMessage = new OutboundMessage(recipient, content);
		outboundMessage.setFrom("SMSBoss");
		
		scheduledSMSs.add(new SMSBossOutboundMessage(outboundMessage, deliveryDate));
	}

	private boolean isSpecialNumber(String number) {
		return "1515".equals(number);
	}
	
	private void validateSMSRequest(String recipient, String content, Date deliveryDate) throws SMSBossValidationException {

		if ( !SMSBossValidator.phoneNumberValid(recipient) )
			throw new SMSBossValidationException(String.format(
					"Invalid recipient  number : '%s'; accepted formats  '+40767802070' / '0767802070'", recipient));

		if (StringUtils.isEmpty(content))
			throw new SMSBossValidationException(String.format("Content cannot be null/empty : '%s'", content));
	}
}
