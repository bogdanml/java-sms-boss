package ro.bogdanml.apps.smsboss;

import java.util.Date;
import java.util.List;

import org.smslib.InboundMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.bogdanml.apps.smsboss.bean.SMSBossInboundMessage;
import ro.bogdanml.apps.smsboss.exception.SMSBossValidationException;


/**
 * 
 * Main interface for sms operations
 * All other components should use this bad-boy for all SMS-related operations
 * @author NagaRampage
 *
 */
@Service
public class SMSBossManager {
	
	
	@Autowired
	private SMSBossScheduler smsScheduler;
	
	@Autowired
	private SMSBossService smsBossService;
	

	public void sendSMS(String recipient, String content) throws SMSBossValidationException {
		smsScheduler.scheduleSmsForDelivery(recipient, content, new Date());
	}
	
	public void sendSMS(String recipient, String content, Date deliveryDate) throws SMSBossValidationException {

		smsScheduler.scheduleSmsForDelivery(recipient, content, deliveryDate);
	}

	public List<SMSBossInboundMessage> readAllSMSMessages() throws SMSBossValidationException {
		
		return smsBossService.readAllSMSMessages();
	}
	
	public List<SMSBossInboundMessage> readAllMessagesForNumber(String senderNumber) throws SMSBossValidationException{
		
		return null;
	}
	
	
	public List<SMSBossInboundMessage> readAllMessagesByInterval(Date startDate,  Date endDate) throws SMSBossValidationException{
		
		return null;
	}
	
	/*public List<SMSBossInboundMessage> readAllMessagesForNumberByInterval(String senderNumber, Date startDate,  Date endDate) throws SMSBossValidationException {
		
		
		List<InboundMessage> myList = null;
		InboundMessage msg =  null;
		
		if(msg.getOriginator().equals(senderNumber)) {
			
			if(msg.getDate().after(startDate) && msg.getDate().before(endDate))
			{
				myList.add(msg);
			}
			
		}
		
		return myList;
	}*/
}


