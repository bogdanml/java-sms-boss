package ro.bogdanml.apps.smsboss;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.AGateway;
import org.smslib.AGateway.Protocols;
import org.smslib.InboundMessage;
import org.smslib.InboundMessage.MessageClasses;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.bogdanml.apps.smsboss.bean.SMSBossInboundMessage;
import ro.bogdanml.apps.smsboss.bean.SMSBossOutboundMessage;
import ro.bogdanml.apps.smsboss.config.SMSBossConfig;
import ro.bogdanml.apps.smsboss.exception.SMSBossException;
import ro.bogdanml.apps.smsboss.exception.SMSBossValidationException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class SMSBossService {
	
	private static final Logger logger  = LoggerFactory.getLogger(SMSBossService.class);

	@Autowired
	private SMSBossConfig smsBossConfig;
	
	private Service smsService;
	private AGateway smsGateway;
	
	public boolean sendSmsMessage(SMSBossOutboundMessage message) {
		
		boolean res = false;

		try {
			
			logger.info("Trying to send SMS message to : {}, content {}",
					message.getMessageTosend().getRecipient(),
					message.getMessageTosend().getText());
			
			res = smsService.sendMessage(message.getMessageTosend());
			
			logger.info("Result : {}", res);
			
		} catch  (Throwable t) {
			logger.error("Error when sending sms message {}, {}", t.getMessage(), t);
			res = false;
		}
		
		return res;
	}
	
	public void deleteAllMessages()  throws SMSBossValidationException {
		
		;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	}
	
	public List<SMSBossInboundMessage> readAllSMSMessages() throws SMSBossValidationException {
		
		List<SMSBossInboundMessage> resList = new ArrayList<SMSBossInboundMessage>();
		Collection<InboundMessage> msgList = new ArrayList<InboundMessage>();
		
		try {
			smsService.readMessages(msgList, MessageClasses.ALL);
		} catch (Throwable t) {
			logger.error("Error when trying to read all messages");
		}
		
		for(InboundMessage im : msgList)  {			
			resList.add(new SMSBossInboundMessage(im));
		}
		
		return resList;
	}
	
	@PostConstruct
	private void init() {
		smsService = new Service();
		
		//Config settings for service
		//smsService.S.SERIAL_POLLING = true;
		
		smsGateway = new  SerialModemGateway(smsBossConfig.getSmsGateway(),
												smsBossConfig.getPortId(),
												Integer.parseInt(smsBossConfig.getBaudRate()),
												"smsBossIndustries",
												"T-1000");
		
		try {
			logger.info("SMSBossService configuration : gateway : {}, portId : [{}], baudrate : [{}], modemId : [{}]",
					smsBossConfig.getSmsGateway(),
					smsBossConfig.getPortId(),
					smsBossConfig.getBaudRate(),
					smsBossConfig.getModemId());
			
			smsGateway.setInbound(Boolean.TRUE);
			smsGateway.setOutbound(Boolean.TRUE);
			smsGateway.setProtocol(Protocols.PDU);
			smsGateway.setFrom("SMSBoss");
			
			smsService.addGateway(smsGateway);
			smsService.startService();
			
			logger.info("SMSBossService started");

			
			//logger.info("Test message : {} ", smsBossConfig.getSendTestSms());
			

		} catch (Throwable e) {
			e.printStackTrace();
			logger.error("Could not configure SMSBoss  service :  {}, {}", e.getMessage(), e);
			throw new SMSBossException(String.format("Error on SMSBossService initialization : %s", e.getMessage()), e );
		}
		
	}
	
}
