package ro.bogdanml.apps.smsboss.constants;

public interface SMSBossConstants {
	
	String SPRING_PROP_PORT_ID = "ro.bogdanml.smsboss.portId";
	String SPRING_PROP_BAUD_RATE = "ro.bogdanml.smsboss.baudrate";
	String SPRING_PROP_GATEWAY_ID  = "ro.bogdanml.smsboss.gateway";
	String SPRING_PROP_MODEM_ID = "ro.bogdanml.smsboss.modemId";
	String SPRING_PROP_DELIVERY_INTERVAL = "ro.bogdanml.smsboss.deliveryInterval";
	String SPRING_PROP_BOOTSMSNUMBER = "ro.bogdanml.smsboss.bootsmsnumber";

}
