package ro.bogdanml.apps.smsboss.exception;

public class SMSBossException extends RuntimeException {
	
	
	public SMSBossException(String message)
	{
		super(message);
	}
	
	public SMSBossException(String message, Throwable t)
	{
		super(message, t);
	}
	

}
