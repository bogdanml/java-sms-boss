package ro.bogdanml.apps.smsboss.exception;

public class SMSBossValidationException extends Exception {

	public SMSBossValidationException(String message) {
		super(message);
	}

	public SMSBossValidationException(String message, Throwable cause) {
		super(message, cause);
	}

}
