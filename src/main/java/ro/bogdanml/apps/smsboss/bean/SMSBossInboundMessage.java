package ro.bogdanml.apps.smsboss.bean;

import org.smslib.InboundMessage;

public class SMSBossInboundMessage {

	private InboundMessage inboundMessage;
	
	public SMSBossInboundMessage(InboundMessage inboundMessage) {
		this.inboundMessage = inboundMessage;				
	}

	public InboundMessage getInboundMessage() {
		return inboundMessage;
	}
	
}
