package ro.bogdanml.apps.smsboss.bean;

import java.util.Date;

import org.smslib.OutboundMessage;

public class SMSBossOutboundMessage {

	private OutboundMessage messageToSend;
	private Date deliveryDate;

	public SMSBossOutboundMessage(OutboundMessage messageToSend) {
		this(messageToSend, new Date());
	}

	public SMSBossOutboundMessage(OutboundMessage messageToSend, Date deliveryDate) {
		
		messageToSend.setFrom("SMSBoss");
		this.messageToSend = messageToSend;
		this.deliveryDate = deliveryDate == null ? 
												new Date() :
												deliveryDate;
	}

	public OutboundMessage getMessageTosend() {
		return this.messageToSend;
	}

	public Date getDeliveDate() {
		return this.deliveryDate;
	}

}
