package ro.bogdanml.apps.smsboss.config.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;

import ro.bogdanml.apps.smsboss.config.SMSBossConfig;
import ro.bogdanml.apps.smsboss.constants.SMSBossConstants;

@Configuration
@PropertySource("classpath:smsboss.properties")
public class SMSBossSpringConfig {

	@Autowired
	private Environment env;
	
	@Bean
	@Scope(BeanDefinition.SCOPE_SINGLETON)
	public SMSBossConfig smsBossConfig() {
		
		
		SMSBossConfig smsBossConfig  = new SMSBossConfig( env.getProperty(SMSBossConstants.SPRING_PROP_PORT_ID),
															env.getProperty(SMSBossConstants.SPRING_PROP_BAUD_RATE),
																env.getProperty(SMSBossConstants.SPRING_PROP_GATEWAY_ID),
																	env.getProperty(SMSBossConstants.SPRING_PROP_MODEM_ID),
															Long.parseLong(env.getProperty(SMSBossConstants.SPRING_PROP_DELIVERY_INTERVAL)),
														env.getProperty(SMSBossConstants.SPRING_PROP_BOOTSMSNUMBER));
		
		return smsBossConfig;
	}
	
}
