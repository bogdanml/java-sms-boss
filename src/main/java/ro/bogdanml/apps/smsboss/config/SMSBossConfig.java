package ro.bogdanml.apps.smsboss.config;

import org.springframework.util.StringUtils;
import ro.bogdanml.apps.smsboss.exception.SMSBossException;

import javax.annotation.PostConstruct;

public class SMSBossConfig {
	
	public SMSBossConfig(String portId,
							String baudRate,
								String smsGateway,
									String modemId,
										Long  deliveryInterval,
											String bootSMSNumber)
	{
		this.portId = portId;
		this.baudRate = baudRate;
		this.smsGateway = smsGateway;
		this.modemId = modemId;
		this.deliveryInterval = deliveryInterval;
		this.bootSMSNumber = bootSMSNumber;
	}
	
	private String portId;
	private String baudRate;
	private String smsGateway;
	private String modemId;
	private Long deliveryInterval;
	private String bootSMSNumber;
	
	@PostConstruct
	private void init() {
		checkConfiguration();
	}
	
	private void checkConfiguration() {

		if( !StringUtils.isEmpty(baudRate)
			|| StringUtils.isEmpty(portId))
		throw new SMSBossException( String.format("Invalid smsboss configuration parameters; portId : %s, baudRate %s",  portId, baudRate));
		
		//Doesn't matter
		modemId = StringUtils.isEmpty(modemId) ? "smsBossModem" : modemId;
		smsGateway = StringUtils.isEmpty(smsGateway) ? "smsBossGateway" : smsGateway;
	}
	
	
	public String getPortId() {
		return portId;
	}

	public String getBaudRate() {
		return baudRate;
	}

	public String getSmsGateway() {
		return smsGateway;
	}
	
	public String getModemId() {
		return modemId;
	}

	public Long getDeliveryInterval() {
		return deliveryInterval;
	}

	public String getBootSMSNumber() {
		return bootSMSNumber;
	}

}
